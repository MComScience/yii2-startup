<?php 
namespace msoft\core\classes;

use Yii;
use msoft\core\models\TbJsScripts;
use yii\web\JsExpression;

class JSQuery {

    public static function generateJS($url,$view){
        $model = TbJsScripts::findOne(['url' => $url]);
        if($model != null){
            //return $view->registerJs($model['code'],$view::POS_END);
            return $view->registerJs($model['code']);
        }else{
            return new JsExpression('alert("Not JS Code");');
        }
    }
}
?>