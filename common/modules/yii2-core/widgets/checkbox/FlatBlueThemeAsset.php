<?php

namespace msoft\widgets\checkbox;

use Yii;
use msoft\widgets\base\AssetBundle;


class FlatBlueThemeAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $depends = [
        'msoft\widgets\checkbox\CheckboxXAsset'
    ];
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets/bootstrap-checkbox-x');
        $this->setupAssets('css', ['css/theme-krajee-flatblue']);
        parent::init();
    }
}
