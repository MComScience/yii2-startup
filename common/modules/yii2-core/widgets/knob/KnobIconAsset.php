<?php


namespace msoft\widgets\knob;


use yii\web\AssetBundle;

class KnobIconAsset extends AssetBundle
{
    public $sourcePath = __DIR__.'/assets';
    public $js = ['js/jquery.knob.icon.min.js'];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
} 