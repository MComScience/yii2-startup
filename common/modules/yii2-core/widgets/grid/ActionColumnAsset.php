<?php

namespace msoft\widgets\grid;

use msoft\widgets\base\AssetBundle;

class ActionColumnAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['js/kv-grid-action']);
        $this->setupAssets('css', ['css/kv-grid-action']);
        parent::init();
    }
}
