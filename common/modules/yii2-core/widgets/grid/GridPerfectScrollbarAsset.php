<?php

namespace msoft\widgets\grid;

use msoft\widgets\base\AssetBundle;

class GridPerfectScrollbarAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/perfect-scrollbar', 'css/perfect-scrollbar-kv']);
        $this->setupAssets('js', ['js/perfect-scrollbar.jquery']);
        parent::init();
    }
}
