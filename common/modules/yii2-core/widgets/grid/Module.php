<?php

namespace msoft\widgets\grid;

use Yii;

/**
 * This module allows global level configurations for the enhanced Krajee [[GridView]]. One can configure the module
 * in their Yii configuration file as shown below:
 *
 * ```php
 * 'modules' => [
 *     'gridview' => [
 *          'class' => 'msoft\widgets\grid\Module',
 *          'downloadAction' => '/gridview/export/download' // your grid export download setting
 *     ]
 * ]
 * ```
 *
 */
class Module extends \msoft\widgets\base\Module
{
    /**
     * The module name for Krajee gridview
     */
    const MODULE = "gridview";

    /**
     * @var string a random salt that will be used to generate a hash string for export configuration.
     */
    public $exportEncryptSalt = 'SET_A_SALT_FOR_YII2_GRID';

    /**
     * @var string|array the action (url) used for downloading exported file
     */
    public $downloadAction = '/gridview/export/download';

    /**
     * @inheritdoc
     */
    protected $_msgCat = 'kvgrid';
}
