<?php

namespace msoft\widgets\spinner;

use Yii;


class SpinnerAsset extends \msoft\widgets\base\AssetBundle
{
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/spin']);
        $this->setupAssets('js', ['js/spin', 'js/jquery.spin']);
        parent::init();
    }
}
