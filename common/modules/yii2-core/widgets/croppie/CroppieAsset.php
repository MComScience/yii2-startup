<?php

namespace msoft\widgets\croppie;

use yii\web\AssetBundle;

class CroppieAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets/Croppie';

    public $css = [
        'croppie.css'
    ];

    public $js = [
        'croppie.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
