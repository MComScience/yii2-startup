<?php

namespace msoft\widgets\touchspin;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use msoft\widgets\base\InputWidget;

class TouchSpin extends InputWidget
{
    /**
     * @inheritdoc
     */
    public $pluginName = 'TouchSpin';

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();
        $this->setPluginOptions();
        $this->registerAssets();
        echo $this->getInput('textInput');
    }

    /**
     * Set the plugin options
     */
    protected function setPluginOptions()
    {
        $css = $this->disabled ? 'btn btn-default disabled' : 'btn btn-default';
        $defaults = [
            'buttonup_class' => $css,
            'buttondown_class' => $css,
            'buttonup_txt' => '<i class="glyphicon glyphicon-forward"></i>',
            'buttondown_txt' => '<i class="glyphicon glyphicon-backward"></i>',
        ];
        $this->pluginOptions = array_replace_recursive($defaults, $this->pluginOptions);
        if (ArrayHelper::getValue($this->pluginOptions, 'verticalbuttons', false) &&
            empty($this->pluginOptions['prefix'])
        ) {
            Html::addCssClass($this->options, 'input-left-rounded');
        }
    }

    /**
     * Registers the needed assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        TouchSpinAsset::register($view);
        $this->registerPlugin($this->pluginName);
    }
}
