<?php

namespace msoft\widgets\touchspin;

use msoft\widgets\base\AssetBundle;

class TouchSpinAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/bootstrap-touchspin']);
        $this->setupAssets('js', ['js/bootstrap-touchspin']);
        parent::init();
    }
}
