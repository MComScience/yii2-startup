<?php

namespace msoft\widgets\editable;

use Yii;
use msoft\widgets\base\AssetBundle;

class EditablePjaxAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['js/editable-pjax']);
        parent::init();
    }
}