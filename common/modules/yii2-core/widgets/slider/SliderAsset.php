<?php

namespace msoft\widgets\slider;

use msoft\widgets\base\AssetBundle;


class SliderAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/bootstrap-slider']);
        $this->setupAssets('js', ['js/bootstrap-slider']);
        parent::init();
    }
}
