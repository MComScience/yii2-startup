<?php

namespace msoft\widgets\fullcalendar;

/**
 * Class PrintAsset
 * @package msoft\widgets\fullcalendar
 */
class PrintAsset extends \yii\web\AssetBundle
{
	/** @var  array The CSS file for the print style */
	public $css = [
		'fullcalendar.print.css',
	];
	/** @var  array The CSS options */
	public $cssOptions = [
		'media' => 'print',
	];
	/** @var  string Bower path for the print settings */
	public $sourcePath = __DIR__ . '/assets/fullcalendar/dist';
}

