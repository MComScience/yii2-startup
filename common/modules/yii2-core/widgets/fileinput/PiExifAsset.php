<?php

/**
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2014 - 2017
 * @package yii2-widgets
 * @subpackage yii2-widget-fileinput
 * @version 1.0.6
 */

namespace msoft\widgets\fileinput;

use msoft\widgets\base\AssetBundle;


class PiExifAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__.'/assets/bootstrap-fileinput');
        $this->setupAssets('js', ['js/plugins/piexif']);
        parent::init();
    }
}
