<?php

/**
 * @copyright Copyright &copy; msoft\widgets Visweswaran, Krajee.com, 2014 - 2016
 * @package yii2-widgets
 * @subpackage yii2-widget-datepicker
 * @version 1.4.2
 */

namespace msoft\widgets\datepicker;

use msoft\widgets\base\AssetBundle;

/**
 * Asset bundle for DatePicker Widget
 *
 * @author msoft\widgets Visweswaran <msoft\widgetsv2@gmail.com>
 * @since 1.0
 */
class DatePickerAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/bootstrap-datepicker3', 'css/datepicker-kv']);
        $this->setupAssets('js', ['js/bootstrap-datepicker', 'js/datepicker-kv']);
        parent::init();
    }
}
