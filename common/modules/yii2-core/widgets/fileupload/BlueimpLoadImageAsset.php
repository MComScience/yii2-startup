<?php

namespace msoft\widgets\fileupload;

use yii\web\AssetBundle;

class BlueimpLoadImageAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets/blueimp-load-image';
    public $js = [
        'js/load-image.all.min.js',
    ];
}
