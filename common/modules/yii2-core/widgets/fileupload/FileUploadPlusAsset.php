<?php

namespace msoft\widgets\fileupload;

use yii\web\AssetBundle;

class FileUploadPlusAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets/blueimp-file-upload';
    public $css = [
        'css/jquery.fileupload.css'
    ];
    public $js = [
        'js/jquery.iframe-transport.js',
        'js/jquery.fileupload-process.js',
        'js/jquery.fileupload-image.js',
        'js/jquery.fileupload-audio.js',
        'js/jquery.fileupload-video.js',
        'js/jquery.fileupload-validate.js'
    ];
    public $depends = [
        'msoft\widgets\fileupload\FileUploadAsset',
        'msoft\widgets\fileupload\BlueimpLoadImageAsset',
        'msoft\widgets\fileupload\BlueimpCanvasToBlobAsset',
    ];
}
