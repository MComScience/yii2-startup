<?php

namespace msoft\widgets\fileupload;

use yii\web\AssetBundle;

class BlueimpTmplAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets/blueimp-tmpl';
    public $js = [
        'js/tmpl.min.js',
    ];
}
