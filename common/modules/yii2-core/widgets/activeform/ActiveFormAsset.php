<?php


namespace msoft\widgets\activeform;
use msoft\widgets\base\AssetBundle;

class ActiveFormAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->depends = array_merge($this->depends, [            
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapPluginAsset'
        ]);
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/activeform']);
        $this->setupAssets('js', ['js/activeform']);
        parent::init();
    }
}
