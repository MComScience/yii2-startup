<?php
namespace msoft\widgets\tinymce;

use yii\web\AssetBundle;

class TinyMceAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets/tinymce';

    public $js = [
        'tinymce.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
