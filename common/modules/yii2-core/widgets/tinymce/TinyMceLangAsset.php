<?php
namespace msoft\widgets\tinymce;

use yii\web\AssetBundle;

class TinyMceLangAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $depends = [
        'msoft\widgets\tinymce\TinyMceAsset'
    ];
}
