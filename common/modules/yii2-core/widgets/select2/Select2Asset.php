<?php

namespace msoft\widgets\select2;

use msoft\widgets\base\AssetBundle;

/**
 * Asset bundle for [[Select2]] Widget.
 *
 */
class Select2Asset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/select2', 'css/select2-addl']);
        $this->setupAssets('js', ['js/select2.full', 'js/select2-krajee']);
        parent::init();
    }
}
