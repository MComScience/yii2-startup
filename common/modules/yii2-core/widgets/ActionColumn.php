<?php

namespace msoft\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use msoft\widgets\grid\ActionColumn as BaseActionColumn;

class ActionColumn extends BaseActionColumn {

	public $pjax_id;

	public $viewOptions = [];

	public $updateOptions = [];

	public $deleteOptions = [];

	/**
	 * Initializes the default button rendering callbacks.
	 */
	protected function initDefaultButtons() {
		if (!isset($this->buttons['view'])) {
			$this->buttons['view'] = function ($url, $model, $key) {
				$options = $this->viewOptions;
				$title = Yii::t('yii', 'View');
				$icon = '<span class="glyphicon glyphicon-eye-open"></span>';
				$label = ArrayHelper::remove($options, 'label', $icon);
				$options = array_replace_recursive(['title' => $title, 'data-pjax' => '0'], $options);
				return Html::a($label, $url, $options);
			};
		}
		if (!isset($this->buttons['update'])) {
			$this->buttons['update'] = function ($url, $model, $key) {
				$options = $this->updateOptions;
                $title = Yii::t('yii', 'Update');
				$icon = '<span class="glyphicon glyphicon-pencil"></span>';
				$label = ArrayHelper::remove($options, 'label', $icon);
				$options = array_replace_recursive(['title' => $title, 'data-pjax' => '0'], $options);
				return Html::a($label, $url, $options);
			};
		}
		if (!isset($this->buttons['delete'])) {
			$this->buttons['delete'] = function ($url, $model, $key) {
				$options = $this->deleteOptions;
                $title = Yii::t('yii', 'Delete');
				$icon = '<span class="glyphicon glyphicon-trash"></span>';
				$label = ArrayHelper::remove($options, 'label', $icon);
				$defaults = [
					'title' => $title, 
					'data-pjax' => 'false',
					'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
					'data-method' => 'post',
				];
				$options = array_replace_recursive($defaults, $options);
				return Html::a($label, $url, $options);
			};
		}
	}

}