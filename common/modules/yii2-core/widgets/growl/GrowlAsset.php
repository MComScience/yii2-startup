<?php

namespace msoft\widgets\growl;

/**
 * Asset bundle for \msoft\widgets\Growl
 *
 */
class GrowlAsset extends \msoft\widgets\base\AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/kv-bootstrap-notify']);
        $this->setupAssets('js', ['js/bootstrap-notify']);
        parent::init();
    }

    /**
     * Adds a theme CSS file
     *
     * @param string $theme the theme name
     *
     * @return msoft\widgets\GrowlAsset object instance
     */
    public function addTheme($theme)
    {
        $ext = YII_DEBUG ? '.css' : '.min.css';
        $this->css[] = "css/{$theme}{$ext}";
        return $this;
    }
}