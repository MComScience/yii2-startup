<?php

namespace msoft\widgets\dynagrid;

use msoft\widgets\base\AssetBundle;

class DynaGridAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['js/kv-dynagrid']);
        $this->setupAssets('css', ['css/kv-dynagrid']);
        parent::init();
    }

}