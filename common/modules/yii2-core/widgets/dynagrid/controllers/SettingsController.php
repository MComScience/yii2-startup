<?php

namespace msoft\widgets\dynagrid\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use msoft\widgets\dynagrid\models\DynaGridSettings;


class SettingsController extends Controller
{
    /**
     * Fetch dynagrid setting configuration
     *
     * @return string
     */
    public function actionGetConfig()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new DynaGridSettings();
        $out = ['status' => '', 'content' => ''];
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $out = ['status' => 'success', 'content' => print_r($model->getDataConfig(), true)];
        }
        return $out;
    }
}