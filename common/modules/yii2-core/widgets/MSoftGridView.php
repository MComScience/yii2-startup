<?php

namespace msoft\core\widgets;

use Yii;
use yii\helpers\Html;
use yii\grid\GridView as BaseGridView;

class GridView extends BaseGridView {

	public $tableOptions = ['class' => 'table table-striped table-bordered table-hover'];
	public $responsiveTable = true;
	public $panel = true;
	public $panelBtn = '';

	/**
	 * Initializes the widget.
	 */
	public function init() {
		parent::init();

		if ($this->panel) {
			$items = ($this->responsiveTable) ? Html::tag('div', '{items}', ['class' => 'table-responsive']) : '{items}';
			$this->layout = <<<EOD
	    <div class="panel panel-default">
		<div class="panel-heading">
			<div class="row">
			    <div class="col-md-6">{summary}</div>
			    <div class="col-md-6 text-right">
				    $this->panelBtn
			    </div>
			</div>
		</div>
		$items
		<div class="panel-footer">{pager}</div>
	    </div>	
EOD;
		}
	}

}
