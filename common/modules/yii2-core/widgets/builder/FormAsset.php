<?php

namespace msoft\widgets\builder;

use msoft\widgets\base\AssetBundle;

class FormAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/form']);
        $this->setupAssets('js', ['js/form']);
        parent::init();
    }
}
