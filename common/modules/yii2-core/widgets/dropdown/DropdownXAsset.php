<?php

namespace msoft\widgets\dropdown;

class DropdownXAsset extends \msoft\widgets\base\AssetBundle
{

    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/dropdown-x']);
        $this->setupAssets('js', ['js/dropdown-x']);
        parent::init();
    }

}