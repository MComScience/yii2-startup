<?php

namespace msoft\widgets\typeahead;

class TypeaheadAsset extends \msoft\widgets\base\AssetBundle
{

    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/typeahead', 'css/typeahead-kv']);
        $this->setupAssets('js', ['js/typeahead.bundle', 'js/typeahead-kv']);
        parent::init();
    }
}
