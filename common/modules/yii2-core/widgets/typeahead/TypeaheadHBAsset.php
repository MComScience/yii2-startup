<?php

namespace msoft\widgets\typeahead;

class TypeaheadHBAsset extends \msoft\widgets\base\AssetBundle
{

    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['js/handlebars']);
        parent::init();
    }
}
