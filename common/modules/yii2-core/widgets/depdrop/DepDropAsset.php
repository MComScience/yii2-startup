<?php

/**
 * @copyright Copyright &copy; msoft\widgets Visweswaran, Krajee.com, 2014 - 2016
 * @package yii2-widgets
 * @subpackage yii2-widget-depdrop
 * @version 1.0.4
 */

namespace msoft\widgets\depdrop;

use msoft\widgets\base\AssetBundle;

/**
 * Asset bundle for Dependent Dropdown widget
 *
 * @author msoft\widgets Visweswaran <msoft\widgetsv2@gmail.com>
 * @since 1.0
 */
class DepDropAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setSourcePath('@msoft/core/assets/dependent-dropdown');
        $this->setupAssets('css', ['css/dependent-dropdown']);
        $this->setupAssets('js', ['js/dependent-dropdown']);
        parent::init();
    }
}
