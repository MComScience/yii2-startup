<?php

namespace msoft\core\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\JsExpression;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "tb_js_scripts".
 *
 * @property integer $id
 * @property string $url
 * @property string $code
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class TbJsScripts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function ($event) {
                    return new Expression('NOW()');
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by'],
                ],
                'value' => function ($event) {
                    return Yii::$app->user->getId();
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'code',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'code',
                ],
                'value' => function ($event) {
                    return new JsExpression($this->code);
                },
            ],
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_AFTER_FIND => 'code',
                ],
                'value' => function ($event) {
                    return new JsExpression($this->code);
                },
            ],
        ];
    }
    public static function tableName()
    {
        return 'core_scripts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'unique'],
            [['url','code'], 'required'],
            [['code'], 'string'],
            [['created_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'code' => 'Code',
            'created_by' => 'ผู้บันทึก',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(\msoft\user\models\Profile::className(), ['user_id' => 'created_by']);
    }
}
