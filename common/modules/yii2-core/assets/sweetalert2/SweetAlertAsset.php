<?php
namespace msoft\core\assets\sweetalert2;

use Yii;
use yii\web\AssetBundle;

class SweetAlertAsset extends AssetBundle
{
    public $sourcePath = '@msoft/core/assets';
    public $css = [
        'sweetalert2/dist/sweetalert2.min.css'
    ];

    public $js = [
        'sweetalert2/dist/sweetalert2.min.js',
        'sweetalert2/dist/confirm.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}