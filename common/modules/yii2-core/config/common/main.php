<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'th',
    'timeZone' => 'Asia/Bangkok',
    'bootstrap' => [
        'msoft\user\Bootstrap',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager', // or use 'yii\rbac\DbManager'
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
            'dateFormat' => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'timeFormat' => 'php:H:i:s',
        ],
        'i18n' => [
            'translations' => [
                /*'core' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@msoft/core/messages', // if advanced application, set @frontend/messages
                    'sourceLanguage' => 'th',
                    'forceTranslation'=>true
                ],
                'kvdynagrid' => [
                    'class'=>'yii\i18n\PhpMessageSource',
                    'basePath'=>'@msoft/widgets/dynagrid/messages',
                    'forceTranslation'=>true
                ],
                'user' => [
                    'class'=>'yii\i18n\PhpMessageSource',
                    'basePath'=>'@msoft/user/messages',
                    'forceTranslation'=>true
                ],*/
            ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                // etc.
            ],
        ]
    ],
    'modules' => [
        'core' => [
            'class' => 'msoft\core\Module'
        ],
        'gridview' =>  [
            'class' => 'msoft\widgets\grid\Module'
        ],
        'user' => [
            'class' => 'msoft\user\Module',
            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['msoft']
        ],
        'pdfjs' => [
            'class' => '\msoft\widgets\pdfjs\Module',
        ],
    ],
    'params' => [
        'icon-framework' => 'fa',  // Font Awesome Icon framework
    ]
];
