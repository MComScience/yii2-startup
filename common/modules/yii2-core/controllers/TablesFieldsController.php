<?php

namespace msoft\core\controllers;

use Yii;
use msoft\core\models\TablesFields;
use msoft\core\models\TablesFieldsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use msoft\core\classes\CoreFunc;
use msoft\helpers\Html;
use yii\data\ActiveDataProvider;
use msoft\core\classes\Generator;
use msoft\widgets\dynamicform\ModelMultiple;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * TablesFieldsController implements the CRUD actions for TablesFields model.
 */
class TablesFieldsController extends Controller {

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index', 'view','list-data'],
						'roles' => ['?', '@'],
					],
					[
						'allow' => true,
						'actions' => [
							'view', 'create', 'update', 'delete', 'deletes',
							'gen-code','rules','get-rules','gii-migration',
							'save-editable','get-rawsql','toggle-update','sort-input',
							'create-dynamic-form','update-dynamic-form','get-column-names'
						],
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	public function beforeAction($action) {
		if (parent::beforeAction($action)) {
			if (in_array($action->id, array('create', 'update'))) {
				
			}
			return true;
		} else {
			return false;
		}
	}

	public function actions(){
		return [
			'toggle-update'=>[
				'class'=>'\msoft\widgets\togglecolumn\actions\ToggleAction',
				'modelClass'=>TablesFields::className()
			]
		];
	  }

	/**
	 * Lists all TablesFields models.
	 * @return mixed
	 */
	public function actionListData() {

		$searchModel = new TablesFieldsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('list-data', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}
	public function actionIndex() {
		//$table = $this->getTable($_GET['table']);

		$searchModel = new TablesFieldsSearch();
		//$searchModel->table_name = $table;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
					'searchModel' => $searchModel,
					'dataProvider' => $dataProvider,
					//'table' => $table,
		]);
	}

	/**
	 * Displays a single TablesFields model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id) {
		if (Yii::$app->getRequest()->isAjax) {
			return $this->renderAjax('view', [
				'model' => $this->findModel($id),
			]);
		} else {
			return $this->render('view', [
				'model' => $this->findModel($id),
			]);
		}
	}

	/**
	 * Creates a new TablesFields model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		if (Yii::$app->getRequest()->isAjax) {
			//$table = $this->getTable($_GET['table']);

			$model = new TablesFields();
			$gii = new Generator();
			//$model->table_name = $table;
			
			if ($model->load(Yii::$app->request->post())) {
				Yii::$app->response->format = Response::FORMAT_JSON;
				$data = Yii::$app->request->post('TablesFields',[]);
				$model->table_name = $data['table_name'];
				
				if ($model->save()) {
					$rules =  $gii->generateRules($data['table_name']);
					$result = [
						'status' => 'success',
						'action' => 'create',
						'message' => Html::getMsgSuccess() . Yii::t('app', 'Data completed.'),
						'data' => $model,
						'rules' => $rules
					];
					return $result;
				} else {
					$result = [
						'status' => 'error',
						'message' => Html::getMsgError() . Yii::t('app', 'Can not create the data.'),
						'data' => $model,
						
					];
					return $result;
				}
			} else {
				return $this->renderAjax('create', [
							'model' => $model,
				]);
			}
		} else {
			$model = new TablesFields();
			//$model->table_name = $table;
			
			if ($model->load(Yii::$app->request->post())) {
				$data = Yii::$app->request->post('TablesFields',[]);
				$model->table_name = $data['table_name'];
				if ($model->save()) {
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Data completed.'));
					return $this->redirect(['index']);
				}else{
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Can not create the data.'));
					return $this->redirect(['index']);
				}
			}else{
				return $this->render('create', [
							'model' => $model,
				]);
			}
			//throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
	}

	public function actionCreateDynamicForm() {
		$models = [new TablesFields];
		
		if (Yii::$app->request->post()) {
			ModelMultiple::loadMultiple($models, Yii::$app->request->post());

			// validate all models
			$valid = ModelMultiple::validateMultiple($models);
			if ($valid) {
				$transaction = \Yii::$app->db->beginTransaction();

				try {
					foreach ($models as $key => $model) {
						$model->input_required = 1;
						if ($model->save(false)) {
							// $transaction->rollBack();
							// break;
						}
					}
					$transaction->commit();
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Data completed.'));
					return $this->redirect(['list-data']);
				} catch (Exception $e) {
					$transaction->rollBack();
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Can not create the data.'));
					return $this->redirect(['list-data']);
				}
			}
		}else{
			return $this->render('_dynamic_form', [
				'models' => $models,
			]);
		}
	}

	public function actionUpdateDynamicForm($id) {
		$models = TablesFields::find()->where(['action_id' => $id])->orderBy('input_order')->all();
		
		if (Yii::$app->request->post()) {
			$oldIDs = ArrayHelper::map($models, 'table_id', 'table_id');
			$models = ModelMultiple::createMultiple(TablesFields::classname(), $models, 'table_id');
			ModelMultiple::loadMultiple($models, Yii::$app->request->post());
			$deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($models, 'table_id', 'table_id')));

			// validate all models
			$valid = ModelMultiple::validateMultiple($models);
			if ($valid) {
				$transaction = \Yii::$app->db->beginTransaction();

				try {
					if (!empty($deletedIDs)) {
						TablesFields::deleteAll(['table_id' => $deletedIDs]);
					}
					foreach ($models as $key => $model) {
						$model->input_required = 1;
						if ($model->save(false)) {
							// $transaction->rollBack();
							// break;
						}
					}
					$transaction->commit();
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Data completed.'));
					return $this->refresh();
				} catch (Exception $e) {
					$transaction->rollBack();
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Can not create the data.'));
					return $this->redirect(['index']);
				}
			}
		}else{
			return $this->render('_dynamic_form', [
				'models' => $models,
			]);
		}
	}

	/**
	 * Updates an existing TablesFields model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id) {
		if (Yii::$app->getRequest()->isAjax) {
			$model = $this->findModel($id);

			if ($model->load(Yii::$app->request->post())) {
				Yii::$app->response->format = Response::FORMAT_JSON;

				if ($model->save()) {

					$result = [
						'status' => 'success',
						'action' => 'update',
						'message' => Html::getMsgSuccess() . Yii::t('app', 'Data completed.'),
						'data' => $model,
					];
					return $result;
				} else {
					$result = [
						'status' => 'error',
						'message' => Html::getMsgError() . Yii::t('app', 'Can not update the data.'),
						'data' => $model,
					];
					return $result;
				}
			} else {
				return $this->renderAjax('update', [
							'model' => $model,
				]);
			}
		} else {
			$model = $this->findModel($id);
			if ($model->load(Yii::$app->request->post())) {
				if ($model->save()) {
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Data completed.'));
					return $this->redirect(['index']);
				}else{
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Can not create the data.'));
					return $this->redirect(['index']);
				}
			}else{
				return $this->render('update', [
							'model' => $model,
				]);
			}
			//throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Deletes an existing TablesFields model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id) {
		if (Yii::$app->getRequest()->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model = $this->findModel($id);
			if ($model->delete()) {
				
				$result = [
					'status' => 'success',
					'action' => 'update',
					'message' => Html::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
					'data' => $id,
				];
				return $result;
			} else {
				$result = [
					'status' => 'error',
					'message' => Html::getMsgError() . Yii::t('app', 'Can not delete the data.'),
					'data' => $id,
				];
				return $result;
			}
		} else {
			throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
	}

	public function actionDeletes() {
		if (Yii::$app->getRequest()->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			if (isset($_POST['selection'])) {
				foreach ($_POST['selection'] as $id) {
					$this->findModel($id)->delete();
				}
				$result = [
					'status' => 'success',
					'action' => 'deletes',
					'message' => Html::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
					'data' => $_POST['selection'],
				];
				return $result;
			} else {
				$result = [
					'status' => 'error',
					'message' => Html::getMsgError() . Yii::t('app', 'Can not delete the data.'),
					'data' => $id,
				];
				return $result;
			}
		} else {
			throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Finds the TablesFields model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return TablesFields the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = TablesFields::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function getTable($table) {
		$tableAlias = CoreFunc::itemAlias('tables_fields', isset($table) ? $table : '');
		if ($tableAlias) {
			return $table;
		} else {
			throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
	}

	public function actionGenCode($id){
		$request = Yii::$app->request;
		if($request->isAjax){
			$model = $this->getAllOptionsTable($id);
			return $this->renderAjax('_code_template', [
				'model' => $model,
			]);
		}else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public static function getAllOptionsTable($table_id) {
	$sql = "SELECT tables_fields.*, 
			core_fields.field_meta, 
			core_fields.field_internal, 
			core_fields.field_class, 
			core_fields.field_name,
			core_fields.field_code
		FROM core_fields INNER JOIN tables_fields ON core_fields.field_code = tables_fields.input_field
		WHERE tables_fields.table_id = :table_id
		ORDER BY input_order, input_label";

	return Yii::$app->db->createCommand($sql, [':table_id' => $table_id])->queryOne();
    }

	public function actionRules(){
		$schema = Yii::$app->db->schema;
		$tables = $schema->getTablenames();
		return $this->render('rules', [
			'tables' => $tables
		]);
	}

	public function actionGetRules($table){
		$rules = [];
		Yii::$app->response->format = Response::FORMAT_JSON;
		if(isset($table) && $table != null){
			$gii = new Generator();
			$rules = $gii->generateRules($table);
		}
		$rule = 'public function rules()<br>';
		$rule .= '{<br>';
		$rule .= '        return ['."\n            " . implode(",\n            ", $rules) . ",\n        ]<br>";
		$rule .= '}';
		return $rule;
		//return implode(',<br>',$rules);
	}

	public function actionSaveEditable($attr) {
        $request = Yii::$app->request;
        if (isset($_POST['hasEditable'])) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $posted = $request->post('TablesFields',[]);
            $Index = $request->post('editableIndex');
            $model = $this->findModel($request->post('editableKey'));
            if (!$model) {
                throw new NotFoundHttpException("The Core Forms was not found.");
            }else {
                $model->{$attr} = $posted[$Index][$attr];
                if($model->save()){
                    return ['output'=> $model->{$attr}, 'message'=>''];
                }else {
                    return ['output'=>'', 'message'=>''];
                }
            }
        }
	}
	
	public function actionGetRawsql() {
		$request = Yii::$app->request;
		if($request->isAjax){
			if($request->post('actionid') != null){
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				$query = TablesFields::find()->where(['action_id' => $request->post('actionid')]);
				$sql  = $query->createCommand()->getRawSql();
				$tableName = 'tables_fields';
				$dataReader = Yii::$app->db->createCommand($sql)->query();
				$data_string = [];
				foreach ( $dataReader as $data ) {
					$itemNames = array_keys ( $data );
					$itemNames = array_map ( "addslashes", $itemNames );
					$items = join ( '`,`', $itemNames );
					$itemValues = array_values ( $data );
					$itemValues[0] = '';
					$itemValues = array_map ( "addslashes", $itemValues );
					$valueString = join ( "','", $itemValues );
					$valueString = "('" . $valueString . "'),";
					$values = "\n" . $valueString;
					
					if ($values != "") {
						$data_string[] = "INSERT INTO `$tableName` (`$items`) VALUES" . rtrim ( $values, "," ) . ";" . PHP_EOL;
					}
				}
				return $data_string;
			}
		}
	}

	public function actionSortInput($id) {
		$request = Yii::$app->request;
		$model = $this->findModel($id);
		$items = [];
		if($request->isGet){
			Yii::$app->response->format = Response::FORMAT_JSON;
			$query = TablesFields::find()->where(['table_name' => $model['table_name'],'action_id' => $model['action_id']])->orderBy('input_order')->all();
			if(count($query) > 0){
				foreach($query as $key => $data){
					$items = \yii\helpers\ArrayHelper::merge($items, [$data['table_id'] => ['content' => $data['input_label'].' / '.$data['table_varname']]]);
				}
			}
			return [
				'title'=> "จัดเรียง",
				'content'=>$this->renderAjax('_sort_input', [
					'model' => $model,
					'items' => $items,
				]),
				'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"]).
							Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
	
			];     
		}elseif($request->isPost){
			Yii::$app->response->format = Response::FORMAT_JSON;
			$keys = $request->post('sort_list');
			$array = explode(",", $keys);
			if(is_array($array)){
				foreach($array as $key => $id){
					$model = TablesFields::findOne($id);
					$model->input_order = $key+1;
					$model->save();
				}
				return [
                    'title'=> "จัดเรียง",
                    'content'=>'<span class="text-success">บันทึกข้อมูลเสร็จเรียบร้อย</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"])
                ];
			}
		}
	}

	public function actionGetColumnNames(){
		$request = Yii::$app->request;
		$schema = Yii::$app->db->schema;
		$out = [];
		if ($request->isPost) {
			$tb_name = $request->post('tb_name');
			if ($tb_name != null) {
				$columns = $schema->getTableSchema($tb_name)->getColumnNames();
				foreach ($columns as $i => $column) {
					$out[$column] = $column;
				}
				echo Json::encode($out);
				return;
			}
		}
		echo Json::encode($out);
	}

}
