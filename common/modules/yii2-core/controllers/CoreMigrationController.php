<?php

namespace msoft\core\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\Json;

class CoreMigrationController extends Controller {

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index'],
						'roles' => ['?', '@'],
					],
					[
						'allow' => true,
						'actions' => ['index','gii-migration','delete','view','delete-all'],
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	public function actionIndex() {
		$path = str_replace('\\', '/', Yii::getAlias('@app/migrations'));
		$filelist = $this->getFileList($path);
		$dataArray = [ ];
		foreach ( $filelist as $id => $data ) {
			
			if(is_array($data)){
				$columns = [];
				foreach($data as $name){
					$columns ['folder_name'] = $id;
					$columns ['filename'] = $name;
					$columns ['size'] = filesize ($path.'/'.$id.'/'.$name);
					$columns ['directory_file'] = $path.'/'.$id.'/'.$name;
					$dataArray [] = $columns;
				}
			}
		}
		$dataProvider = new ArrayDataProvider([
			'allModels' => $dataArray,
			'pagination' => [
				'pageSize' => 50,
			],
			'key' => 'filename'
		]);
		return $this->render('index',[
			'dataProvider' => $dataProvider
		]);
	}

	public function actionGiiMigration(){
		$request = Yii::$app->request;
		$db = Yii::$app->db;
		$tbnames = $db->schema->getTableNames();
		if(count($tbnames) > 0){
			$dir = str_replace('\\', '/', Yii::getAlias('@app/migrations/').gmdate('dmY_H0101'));
			if(!is_dir($dir)){
				FileHelper::createDirectory($dir,0777);
			}
			if(is_dir($dir)){
				foreach($tbnames as $name){
					$generator = new \msoft\gii\migration\Generator([
						'migrationTime' => gmdate('dmY_H0101'),
						'tableName' => $name,
						'migrationName' => $name,
						'migrationPath' => $dir
					]);
					$files = $generator->generate();
					file_put_contents(\yii\helpers\ArrayHelper::getValue($files[0],'path'), \yii\helpers\ArrayHelper::getValue($files[0],'content'));
				}
				\Yii::$app->session->setFlash('success', 'Generate Completed!');
				return $this->redirect(['index']);
			}else{
				return $this->redirect(['index']);
			}
		}
	}

	protected function getFileList($path) {
		$filelist = [];
		
		if(!is_dir(Yii::getAlias('@app/migrations'))){
			FileHelper::createDirectory(Yii::getAlias('@app/migrations'),0777);
		} 
		$cdir = scandir($path);
		foreach ($cdir as $key => $value) 
		{ 
			if (!in_array($value,array(".",".."))) 
			{ 
				if (is_dir($path . DIRECTORY_SEPARATOR . $value)) 
				{ 
					$filelist = ArrayHelper::merge($filelist,[$value => str_replace('\\', '/', $this->getFileList($path . DIRECTORY_SEPARATOR . $value))]);
				} 
				else 
				{ 
					$filelist = ArrayHelper::merge($filelist, [str_replace('\\', '/', $value)]);
				} 
			} 
		}
		return $filelist;
	}

	public function actionDelete($id){
		if(file_exists(Yii::getAlias('@app/migrations/').$id)){
			@unlink(Yii::getAlias('@app/migrations/').$id);
			\Yii::$app->session->setFlash('success', 'Deleted!');
			return $this->redirect(['index']);
		}else{
			\Yii::$app->session->setFlash('error', 'file not exist.');
			return $this->redirect(['index']);
		}
	}

	public function actionView($id,$filename){
		$request = Yii::$app->request;
        if($request->isAjax){
			$content = '';
			Yii::$app->response->format = Response::FORMAT_JSON;
			if(file_exists(Yii::getAlias('@app/migrations/').$id.'/'.$filename)){
				$content = file_get_contents(Yii::getAlias('@app/migrations/').$id.'/'.$filename);
				return [
                    'title'=> "Code File # ".$id,
                    'content'=>$this->renderAjax('view', [
                        'content' => $content,
						'id' => $id
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default','data-dismiss'=>"modal"])
                ];
			}else{
				throw new NotFoundHttpException('file not exist.');
			}
        }else{
            return $this->render('view', [
                'content' => $content,
				'id' => $id
            ]);
        }
	}

	public function actionDeleteAll(){
		$request = Yii::$app->request;
        if($request->isAjax){
			Yii::$app->response->format = Response::FORMAT_JSON;
			if(is_dir(Yii::getAlias('@app/migrations'))){
				FileHelper::removeDirectory(Yii::getAlias('@app/migrations'));
			}
			return 'Success!';
		}
	}

}