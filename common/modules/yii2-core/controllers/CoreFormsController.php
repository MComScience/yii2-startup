<?php

namespace msoft\core\controllers;

use Yii;
use msoft\core\models\CoreForms;
use msoft\core\models\CoreFormsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use msoft\helpers\Html;
use yii\helpers\Json;
use msoft\widgets\dynamicform\ModelMultiple;
use yii\helpers\ArrayHelper;

/**
 * CoreFormsController implements the CRUD actions for CoreForms model.
 */
class CoreFormsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions(){
        return [
            'toggle-update'=>[
                'class'=>'\msoft\widgets\togglecolumn\actions\ToggleAction',
                'modelClass'=>CoreForms::className()
            ]
        ];
    }

    /**
     * Lists all CoreForms models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CoreFormsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CoreForms model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CoreForms model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CoreForms();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Data completed.'));
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CoreForms model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function actionUpdate($id) {
		if (Yii::$app->getRequest()->isAjax) {
			$model = $this->findModel($id);

			if ($model->load(Yii::$app->request->post())) {
				Yii::$app->response->format = Response::FORMAT_JSON;

				if ($model->save()) {

					$result = [
						'status' => 'success',
						'action' => 'update',
						'message' => Html::getMsgSuccess() . Yii::t('app', 'Data completed.'),
						'data' => $model,
					];
					return $result;
				} else {
					$result = [
						'status' => 'error',
						'message' => Html::getMsgError() . Yii::t('app', 'Can not update the data.'),
						'data' => $model,
					];
					return $result;
				}
			} else {
				return $this->renderAjax('update', [
							'model' => $model,
				]);
			}
		} else {
			$model = $this->findModel($id);
			if ($model->load(Yii::$app->request->post())) {
				if ($model->save()) {
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Data completed.'));
					return $this->redirect(['index']);
				}else{
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Can not create the data.'));
					return $this->redirect(['index']);
				}
			}else{
				return $this->render('update', [
							'model' => $model,
				]);
			}
		}
	}

    /**
     * Deletes an existing CoreForms model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
     public function actionDelete($id) {
		if (Yii::$app->getRequest()->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$model = $this->findModel($id);
			if ($model->delete()) {
				
				$result = [
					'status' => 'success',
					'action' => 'update',
					'message' => Html::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
					'data' => $id,
				];
				return $result;
			} else {
				$result = [
					'status' => 'error',
					'message' => Html::getMsgError() . Yii::t('app', 'Can not delete the data.'),
					'data' => $id,
				];
				return $result;
			}
		} else {
			throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
	}

    /**
     * Finds the CoreForms model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CoreForms the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreForms::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChildColumns() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $schema = Yii::$app->db->schema;
            $columns = $schema->getTableSchema($id)->getColumnNames();
            $selected  = null;
            if ($id != null && count($columns) > 0) {
                $selected = '';
                foreach ($columns as $i => $name) {
                    $out[] = ['id' => $name, 'name' => $name];
                    if ($i == 0) {
                        $selected = $name;
                    }
                }
                // Shows how you can preselect a value
                echo Json::encode(['output' => $out, 'selected'=>$selected]);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected'=>'']);
    }

    public function actionDeletes() {
		if (Yii::$app->getRequest()->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			if (isset($_POST['selection'])) {
				foreach ($_POST['selection'] as $id) {
					$this->findModel($id)->delete();
				}
				$result = [
					'status' => 'success',
					'action' => 'deletes',
					'message' => Html::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
					'data' => [],
				];
				return $result;
			} else {
				$result = [
					'status' => 'error',
					'message' => Html::getMsgError() . Yii::t('app', 'Can not delete the data.'),
					'data' => [],
				];
				return $result;
			}
		} else {
			throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
	}

    public function actionSaveEditable($attr) {
        $request = Yii::$app->request;
        if (isset($_POST['hasEditable'])) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $posted = $request->post('CoreForms',[]);
            $Index = $request->post('editableIndex');
            $model = $this->findModel($request->post('editableKey'));
            if (!$model) {
                throw new NotFoundHttpException("The Core Forms was not found.");
            }else {
                $model->{$attr} = $posted[$Index][$attr];
                if($model->save()){
                    return ['output'=> $model->{$attr}, 'message'=>''];
                }else {
                    return ['output'=>'', 'message'=>''];
                }
            }
        }
    }

    public function actionGetRawsql($actionid) {
		$request = Yii::$app->request;
		if($request->isAjax){
			if($actionid != null){
                Yii::$app->response->format = Response::FORMAT_JSON;
				$query = CoreForms::find()->where(['action_id' => $actionid]);
				$sql  = $query->createCommand()->getRawSql();
				$tableName = 'core_forms';
				$dataReader = Yii::$app->db->createCommand($sql)->query();
				$data_string = [];
				foreach ( $dataReader as $data ) {
					$itemNames = array_keys ( $data );
					$itemNames = array_map ( "addslashes", $itemNames );
					$items = join ( '`,`', $itemNames );
					$itemValues = array_values ( $data );
					$itemValues[0] = '';
					$itemValues = array_map ( "addslashes", $itemValues );
					$valueString = join ( "','", $itemValues );
					$valueString = "('" . $valueString . "'),";
					$values = "\n" . $valueString;
					
					if ($values != "") {
						$data_string[] = "INSERT INTO `$tableName` (`$items`) VALUES" . rtrim ( $values, "," ) . ";" . PHP_EOL;
					}
                }
                // return [
                //     'title' => "RawSql",
                //     'content' => '<pre>'.implode("", $data_string).'</pre>',
                //     'footer' =>
                //     Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]) .
                //     Html::button('Save', ['class' => 'btn btn-success', 'type' => "submit"])
                // ];
				return implode("", $data_string);
			}
		}
    }
    
    public function actionCreateDynamicForm() {
		$models = [new CoreForms];
		
		if (Yii::$app->request->post()) {
			ModelMultiple::loadMultiple($models, Yii::$app->request->post());

			// validate all models
			$valid = ModelMultiple::validateMultiple($models);
			if ($valid) {
				$transaction = \Yii::$app->db->beginTransaction();

				try {
					foreach ($models as $key => $model) {
                        $model->status = $model->isNewRecord ? '1' : $model['status'];
						if ($model->save(false)) {
							// $transaction->rollBack();
							// break;
						}
					}
					$transaction->commit();
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Data completed.'));
					return $this->redirect(['index']);
				} catch (Exception $e) {
					$transaction->rollBack();
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Can not create the data.'));
					return $this->redirect(['index']);
				}
			}
		}else{
			return $this->render('_dynamic_form', [
				'models' => $models,
			]);
		}
    }

    public function actionUpdateDynamicForm($id) {
		$models = CoreForms::find()->where(['action_id' => $id])->orderBy('order')->all();
		
		if (Yii::$app->request->post()) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$oldIDs = ArrayHelper::map($models, 'form_id', 'form_id');
			$models = ModelMultiple::createMultiple(CoreForms::classname(), $models, 'form_id');
			ModelMultiple::loadMultiple($models, Yii::$app->request->post());
			$deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($models, 'form_id', 'form_id')));

			// validate all models
			$valid = ModelMultiple::validateMultiple($models);
			if ($valid) {
				$transaction = \Yii::$app->db->beginTransaction();

				try {
					if (!empty($deletedIDs)) {
						CoreForms::deleteAll(['form_id' => $deletedIDs]);
					}
					foreach ($models as $key => $model) {
                        $model->status = $model->isNewRecord ? '1' : $model['status'];
						if ($model->save(false)) {
							// $transaction->rollBack();
							// break;
						}
					}
					$transaction->commit();
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Data completed.'));
					return Yii::$app->session->getFlash('success');
					//return $this->refresh();
				} catch (Exception $e) {
					$transaction->rollBack();
					\Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Can not create the data.'));
					//return $this->redirect(['index']);
				}
			}
		}else{
			return $this->render('_dynamic_form', [
				'models' => $models,
			]);
		}
	}
    
    public function actionGetColumnNames(){
		$request = Yii::$app->request;
		$schema = Yii::$app->db->schema;
		$out = [];
		if ($request->isPost) {
			$tb_name = $request->post('tb_name');
			if ($tb_name != null) {
				$columns = $schema->getTableSchema($tb_name)->getColumnNames();
				foreach ($columns as $i => $column) {
					$out[$column] = $column;
				}
				echo Json::encode($out);
				return;
			}
		}
		echo Json::encode($out);
	}
}
