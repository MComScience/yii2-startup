<?php

namespace msoft\core\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
/**
 * Default controller for the `editor` module
 */
class CoreEditorController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        if($request->isPost){
            if(file_exists($request->post('dir'))){
                @chmod($request->post('dir'), 0777);
                if (@file_put_contents($request->post('dir'), $request->post('Code')) === false) {
                    return Json::encode("Unable to write the file.");
                }else{
                    $mask = @umask(0);
                    @mkdir(dirname($request->post('dir')), 00777, true);//0755
                    @file_put_contents($request->post('dir'), $request->post('Code'));
                    @umask($mask);
                    return Json::encode("Completed!");
                }
            }else{
                return Json::encode("no such file in directory.");
            }
        }else{
            $dirfrontend    = Yii::getAlias('@frontend');
            $dirbackend    = Yii::getAlias('@backend');
            $dircommon    = Yii::getAlias('@common');

            // $backend = $this->dirToArray($dirbackend);
            // $frontend = $this->dirToArray($dirfrontend);
            // $common = $this->dirToArray($dircommon);
            
            return $this->render('index',[
                // 'frontend' => $frontend,
                // 'backend' => $backend,
                // 'common' => $common
            ]);
        }
    }

    private function dirToArray($dir){
        $result = [];
        $ignore = [
            ".",
            "..",
            "runtime",
            'tests',
            'web',
            'migrations',
            'codeception.yml',
            '.gitignore',
            'vendor',
            'console',
            'environments',
            'vagrant',
            'common',
        ];
        $cdir = scandir($dir); 
        foreach ($cdir as $key => $value) 
        { 
            if (!in_array($value,$ignore)) 
            { 
                if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
                { 
                    //$result[$value] = $this->dirToArray($dir . DIRECTORY_SEPARATOR . $value);
                    $result[] = [
                                    "text" => $value,
                                    "children" => $this->dirToArray($dir . DIRECTORY_SEPARATOR . $value),
                                    'data' => str_replace('\\', '/', $dir . DIRECTORY_SEPARATOR.$value),
                                ];
                } 
                else 
                { 
                    $result[] = [
                        'text' => $value,
                        "icon" => "fa fa-file-text-o icon-state-default",
                        'data' => str_replace('\\', '/',$dir .DIRECTORY_SEPARATOR.$value),
                        'type' => 'file'
                    ]; 
                } 
            } 
        }
        return $result;
    }

    public function  actionGetFileContent(){
        $request = Yii::$app->request;
        if($request->isAjax){
            $content = '';
            if(file_exists($request->post('path'))){
                $content = file_get_contents($request->post('path'));
            }
            return (string)$content;
        }
    }
    public function actionCreateFile(){
        $request = Yii::$app->request;
        $result = '';
        if($request->isAjax){
            if(!file_exists($request->post('dir'))){
                $mask = @umask(0);
                @mkdir(dirname($request->post('dir')), 00777, true);
                @file_put_contents($request->post('dir'), "<?php ?>");
                @chmod($request->post('dir'), 0777);
                @umask($mask);
                $result = 'Created!';
            }else{
                $result = 'A file already!';
            }
            return Json::encode($result);
        }
    }

    public function actionCreateFolder(){
        $request = Yii::$app->request;
        $result = '';
        if($request->isAjax){
            if(!is_dir($request->post('dir'))){
                if (is_dir($request->post('dir'))) {
                    chmod($request->post('parentnode'), 0777);
                }
                FileHelper::createDirectory($request->post('dir'),00777);
                $result = 'Created!';
            }else{
                $result = 'A folder already!';
            }
            return Json::encode($result);
        }
    }

    public function actionDeleteNode(){
        $request = Yii::$app->request;
        $result = '';
        if($request->isAjax){
            $data = $request->post('data',[]);

            if($data['type'] == 'file' && file_exists($request->post('dir'))){
                unlink($request->post('dir'));
                $result = 'Deleted!';
            }

            if($data['type'] == 'default' && is_dir($request->post('dir'))){
                FileHelper::removeDirectory($request->post('dir'));
                $result = 'Deleted!';
            }
            return Json::encode($result);
        }
    }

    public function actionRenameNode(){
        $request = Yii::$app->request;
        $result = '';
        if($request->isAjax){
            $data = $request->post('data',[]);

            if($data['type'] == 'file' && file_exists($request->post('oldname'))){
                $mask = @umask(0);
                @chmod($request->post('oldname'), 0777); 
                @rename($request->post('oldname'),$request->post('newname'));
                @chmod($request->post('newname'), 0777);
                @umask($mask);
                $result = [
                    'status' => 'Completed!',
                    'children' => []
                ];
            }elseif($data['type'] == 'default' && is_dir($request->post('oldname'))){
                $mask = @umask(0);
                @chmod($request->post('oldname'), 0777);
                @rename($request->post('oldname'),$request->post('newname'));
                @chmod($request->post('newname'), 0777);
                @umask($mask);
                $children = $this->dirToArray($request->post('newname'));
                $result = [
                    'status' => 'Completed!',
                    'children' => $children
                ];
            }else{
                $result = [
                    'status' => 'Rename error!',
                    'children' => []
                ];
            }
            return Json::encode($result);
        }
    }

    public function actionPasteNode(){
        $request = Yii::$app->request;
        $result = '';
        $children = [];
        if($request->isAjax){
            $source = $request->post('source',[]);
            $dest = $request->post('destination',[]);
            if( is_array($source) && count($source) > 0 ){
                foreach($source as $key => $data){
                    #File
                    if($data['type'] == 'file'){
                        $file = $dest['data'].'/'.$data['text'];

                        if(file_exists($file)){
                            return Json::encode("A file already! failed to copy.");
                        }
                        if ( !copy($data['data'], $file)) {
                            break;
                            return Json::encode("failed to copy.");
                        }
                    }
                    #Folder
                    if($data['type'] == 'default'){
                        $folder = $dest['data'].'/'.$data['text'];
                        if(is_dir($folder)){
                            return Json::encode("A folder already! failed to copy.");
                        }else{
                            FileHelper::copyDirectory($data['data'],$folder,['dirMode' => 00777]);
                            $children = $this->dirToArray($dest['data']);
                        }
                    }elseif($dest['type'] == 'default'){
                        $children = $this->dirToArray($dest['data']);
                    }
                }
                return Json::encode([
                    'status' => 'Copied!',
                    'children' => $children
                ]);
            }else{
                return Json::encode([
                    'status' => 'failed to copy.',
                    'children' => $children
                ]);
            }
        }
    }

    public function actionLoadData(){
        $request = Yii::$app->request;
        $result = '';
        $children = [];
        if($request->isAjax){
            $dirfrontend    = Yii::getAlias('@frontend');
            $dirbackend    = Yii::getAlias('@backend');
            $dircommon    = Yii::getAlias('@common');

            $backend = $this->dirToArray($dirbackend);
            $frontend = $this->dirToArray($dirfrontend);
            $common = $this->dirToArray($dircommon);
            $children = [
                            ['text' => 'backend','children' => $backend],
                            ['text' => 'common','children' => $common],
                            ['text' => 'frontend','children' => $frontend],
            ]; 
            return Json::encode([
                    'status' => 'Success',
                    'children' => $children
                ]);
        }
    }
}
