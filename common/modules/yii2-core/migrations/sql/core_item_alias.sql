/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : db_nsdu

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2017-09-28 10:38:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for core_item_alias
-- ----------------------------
DROP TABLE IF EXISTS `core_item_alias`;
CREATE TABLE `core_item_alias` (
  `item_code` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT 'ID',
  `item_name` varchar(200) CHARACTER SET utf8 NOT NULL COMMENT 'Name',
  `item_data` longtext CHARACTER SET utf8 NOT NULL COMMENT 'Data',
  PRIMARY KEY (`item_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of core_item_alias
-- ----------------------------
INSERT INTO `core_item_alias` VALUES ('autoload', 'Auto load', '[\'yes\' => \'Yes\',\'no\' => \'No\']');
INSERT INTO `core_item_alias` VALUES ('boolean_list', 'Boolean List', '[\'false\'=>\'False\', \'true\'=>\'True\']');
INSERT INTO `core_item_alias` VALUES ('btn_class', 'Button class', '[\'btn-default\'=>\'Default\', \'btn-primary\'=>\'Primary\', \'btn-success\'=>\'Success\', \'btn-info\'=>\'Info\', \'btn-warning\'=>\'Warning\', \'btn-danger\'=>\'Danger\', \'btn-link\'=>\'Link\']');
INSERT INTO `core_item_alias` VALUES ('btn_size', 'Button size', '[\'btn-lg\'=>\'Large\', \'btn-sm\'=>\'Small\', \'btn-sm\'=>\'Small\', \'btn-xs\'=>\'Extra small\']');
INSERT INTO `core_item_alias` VALUES ('data_type', 'Data Type', '[\'JSON\'=>\'JSON\', \'HTML\'=>\'HTML\', \'TEXT\'=>\'TEXT\', \'XML\'=>\'XML\', \'SCRIPT\'=>\'SCRIPT\']');
INSERT INTO `core_item_alias` VALUES ('field_index', 'Index', '[\'INDEX\'=>\'INDEX\', \'FULLTEXT\'=>\'FULLTEXT\']');
INSERT INTO `core_item_alias` VALUES ('field_type', 'Field Type', '[\'INT\'=>\'INT\', \'VARCHAR\'=>\'VARCHAR\', \'TEXT\'=>\'TEXT\', \'DATE\'=>\'DATE\', \'DATETIME\'=>\'DATETIME\', \'DOUBLE\'=>\'DOUBLE\', \'TINYINT\'=>\'TINYINT\']');
INSERT INTO `core_item_alias` VALUES ('gen_group', 'Generate group', '[\'HTML\' => \'HTML\', \'JS\' => \'JS\', \'COMPONENT\' => \'Component\', \'WIDGET\' => \'Widget\', \'PHP\' => \'PHP\', \'CLASS\' => \'Class\', \'SQL\' => \'SQL\', \'YII\' => \'Yii\', \'YII2\' => \'Yii2\', \'OTHER\' => \'Other\']');
INSERT INTO `core_item_alias` VALUES ('gen_type', 'GenType', '[\'php\' => \'PHP\',\'ajax\' => \'Ajax\',\'datatables\' => \'Datatables\']');
INSERT INTO `core_item_alias` VALUES ('method_list', 'Method List', '[\'POST\'=>\'POST\', \'GET\'=>\'GET\', \'PUT\'=>\'PUT\']');
INSERT INTO `core_item_alias` VALUES ('qtype', 'ประเภทคำถาม', '[1=>\'คำถามแบบให้คะแนน\',2=>\'คำถามแบบคำตอบเดียว\',3=>\'คำถามแบบหลายคำตอบ\',4=>\'คำถามแบบอัตนัย\',5=>\'หัวข้อคำถาม\']');
INSERT INTO `core_item_alias` VALUES ('tables_fields', 'Tables Fields', '[\'profile\'=>\'Profile\', \'post\'=>\'Post\']');
INSERT INTO `core_item_alias` VALUES ('tb_usertype', 'tb_usertype', '[\'1\' => \'นักศึกษา\',\'2\' => \'อาจารย์\',\'3\' => \'เจ้าหน้าที่\',\'4\' => \'ศิษย์เก่า\',\'5\' => \'อาจารย์พิเศษ\']');
INSERT INTO `core_item_alias` VALUES ('text_align', 'Text Align', '[\'center\'=>\'Center\', \'left\'=>\'Left\', \'right\'=>\'Right\', \'justify\'=>\'Justify\']');
INSERT INTO `core_item_alias` VALUES ('user_sex', 'user_sex', '[\'1\' => \'ชาย\',\'2\' => \'หญิง\']');
INSERT INTO `core_item_alias` VALUES ('utitle', 'คำนำหน้า', '[\'1\' => \'นาย\',\'2\' => \'นาง\',\'3\' => \'นางสาว\']');
INSERT INTO `core_item_alias` VALUES ('widgets', 'Widgets', '[\'1\' => \'Select2\',\'2\' => \'Typeahead\',\'3\' => \'TimePicker\',\'4\' => \'DateTimePicker\',\'5\' => \'TouchSpin\',\'6\' => \'ColorInput\',\'7\' => \'RangeInput\',\'8\' => \'SwitchInput\',\'9\' => \'StarRating\',\'10\' => \'DepDrop\',\'11\' => \'CheckboxX\',\'12\' => \'DatePicker\',\'13\' => \'DateRangePicker\',\'14\' => \'DateControl\',\'15\' => \'Editable\',\'16\' => \'CKEditor\',\'17\' => \'Upload\',\'18\' => \'CodeMirror\']');
