/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : yii2-startup

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2017-09-28 11:21:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tables_fields
-- ----------------------------
/*
DROP TABLE IF EXISTS `tables_fields`;
CREATE TABLE `tables_fields` (
  `table_id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(50) NOT NULL,
  `table_varname` varchar(50) NOT NULL,
  `table_field_type` varchar(50) NOT NULL,
  `table_length` varchar(10) NOT NULL,
  `table_default` text NOT NULL,
  `table_index` varchar(50) NOT NULL,
  `input_field` varchar(20) NOT NULL,
  `input_label` varchar(100) NOT NULL,
  `input_hint` text NOT NULL,
  `input_specific` text NOT NULL,
  `input_data` text NOT NULL,
  `input_required` smallint(1) NOT NULL,
  `input_validate` text NOT NULL,
  `input_meta` text NOT NULL,
  `input_order` smallint(3) NOT NULL,
  `action_id` varchar(255) DEFAULT NULL,
  `begin_html` text,
  `end_html` text,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
*/
-- ----------------------------
-- Records of tables_fields
-- ----------------------------
INSERT INTO `tables_fields` VALUES ('1', 'core_scripts', 'code', 'TEXT', '', '', '', 'CodeMirror', 'Code', '', 'a:1:{s:10:\"showLabels\";b:0;}', '', '0', '', 'a:2:{s:7:\"options\";a:2:{s:4:\"rows\";i:5;s:2:\"id\";s:4:\"code\";}s:13:\"clientOptions\";a:5:{s:11:\"lineNumbers\";b:1;s:13:\"matchBrackets\";b:1;s:15:\"styleActiveLine\";b:1;s:5:\"theme\";s:8:\"ambiance\";s:4:\"mode\";s:15:\"text/javascript\";}}', '2', '/core/core-scripts/update', '<div class=\"form-group\">\r\n        <label class=\"col-sm-1 control-label\">Code</label>\r\n        <div class=\"col-sm-11\">', '	</div>\r\n</div>', '2017-09-11 12:04:56', '3', '2017-08-07 13:43:57', '3', '1');
INSERT INTO `tables_fields` VALUES ('24', 'core_scripts', 'url', 'TEXT', '255', '', '', 'Typeahead', 'Url', '', 'a:1:{s:10:\"showLabels\";b:0;}', '', '0', '', 'a:3:{s:7:\"options\";a:1:{s:11:\"placeholder\";s:16:\"Filter Action...\";}s:13:\"pluginOptions\";a:2:{s:9:\"highlight\";b:1;s:9:\"minLength\";i:0;}s:7:\"dataset\";a:1:{i:0;a:2:{s:8:\"prefetch\";s:29:\"/core/core-scripts/get-routes\";s:5:\"limit\";i:20;}}}', '1', '/core/core-scripts/update', '<div class=\"form-group\">\r\n	<label class=\"col-sm-1 control-label\">Url</label>\r\n        <div class=\"col-sm-11\">', '</div>\r\n</div>', '2017-08-31 00:32:02', '3', '2017-08-07 13:49:48', '3', '1');
