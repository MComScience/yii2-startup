/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : yii2-startup

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2017-09-28 13:37:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for core_forms
-- ----------------------------
DROP TABLE IF EXISTS `core_forms`;
CREATE TABLE `core_forms` (
  `form_id` int(11) NOT NULL AUTO_INCREMENT,
  `row` varchar(255) NOT NULL,
  `action_id` varchar(255) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `attributes` varchar(255) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `visible` varchar(255) DEFAULT NULL,
  `labelSpan` int(11) DEFAULT NULL,
  `labelOptions` text,
  `hint` text,
  `items` text,
  `value` text,
  `widgetClass` varchar(255) DEFAULT NULL,
  `options` text,
  `fieldConfig` text,
  `columnOptions` text,
  `columns` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of core_forms
-- ----------------------------
INSERT INTO `core_forms` VALUES ('1', '1', 'DemoForm', 'tables_fields', 'table_name', '', 'textInput', '', null, 'a:1:{s:5:\"class\";s:22:\"col-sm-2 control-label\";}', '', '', '', '', '[\'placeholder\' => \'Table name\']', 'a:1:{s:10:\"showLabels\";b:0;}', 'a:1:{s:7:\"colspan\";i:4;}', '', '1', '1', '1', '1', '2017-09-28 13:36:34', '2017-09-28 13:36:34');
INSERT INTO `core_forms` VALUES ('2', '1', 'DemoForm', 'tables_fields', 'table_varname', '', 'textInput', '', null, 'a:1:{s:5:\"class\";s:22:\"col-sm-1 control-label\";}', '', '', '', '', '[\'placeholder\' => \'Table varname\']', 'a:1:{s:10:\"showLabels\";b:0;}', 'a:1:{s:7:\"colspan\";i:4;}', '', '2', '1', '1', '1', '2017-09-28 13:36:34', '2017-09-28 13:36:34');
INSERT INTO `core_forms` VALUES ('3', '2', 'DemoForm', 'tables_fields', 'table_field_type', '', 'widget', '', null, 'a:1:{s:5:\"class\";s:22:\"col-sm-2 control-label\";}', '<span class=\"text-danger\">**ขนาดไฟล์: ไม่เกิน 50MB/ไฟล์</span> ,ชนิดไฟล์: gif, jpeg, png, doc, docx, pdf, zip, rar, xls, xlsx, ppt, pptx', '', '', '\\trntv\\filekit\\widget\\Upload::classname()', '[\r\n  \'url\' => [\'upload-file\'],\r\n  \'sortable\' => true,\r\n  \'maxFileSize\' => 500 * 1024 * 1024, // 50 MiB\r\n  \'maxNumberOfFiles\' => 20,\r\n  \'acceptFileTypes\' => new \\yii\\web\\JsExpression(\'/(\\.|\\/)(gif|jpe?g|png|doc|docx|pdf|zip|rar|xls|xlsx|ppt|pptx)$/i\'),\r\n  \'clientOptions\' => [ \r\n    \'fail\' => new \\yii\\web\\JsExpression(\'function(e, data) { AppNotify.Error(data.errorThrown); }\'),\r\n ]\r\n]', 'a:1:{s:10:\"showLabels\";b:0;}', 'a:1:{s:7:\"colspan\";i:9;}', '', '3', '1', '1', '1', '2017-09-28 13:36:34', '2017-09-28 13:36:34');
INSERT INTO `core_forms` VALUES ('4', '3', 'DemoForm', 'tables_fields', 'table_length', '', 'widget', '', null, 'a:1:{s:5:\"class\";s:22:\"col-sm-2 control-label\";}', '', '', '', '\\msoft\\widgets\\CKEditor::classname()', '', 'a:1:{s:10:\"showLabels\";b:0;}', 'a:1:{s:7:\"colspan\";i:9;}', '', '4', '1', '1', '1', '2017-09-28 13:36:34', '2017-09-28 13:36:34');
INSERT INTO `core_forms` VALUES ('5', '4', 'DemoForm', 'tables_fields', 'table_default', '', 'widget', '', null, 'a:1:{s:5:\"class\";s:22:\"col-sm-2 control-label\";}', '', '', '', '\\msoft\\widgets\\StarRating::classname()', '', 'a:1:{s:10:\"showLabels\";b:0;}', 'a:1:{s:7:\"colspan\";i:4;}', '', '5', '1', '1', '1', '2017-09-28 13:36:34', '2017-09-28 13:36:34');
