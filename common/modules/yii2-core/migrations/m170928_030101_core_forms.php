<?php

use yii\db\Schema;

class m170928_030101_core_forms extends \yii\db\Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('core_forms', [
            'form_id' => $this->primaryKey(),
            'row' => $this->string(255)->notNull(),
            'action_id' => $this->string(255)->notNull(),
            'table_name' => $this->string(255)->notNull(),
            'attributes' => $this->string(255)->notNull(),
            'label' => $this->string(255),
            'type' => $this->string(255)->notNull(),
            'visible' => $this->string(255),
            'labelSpan' => $this->integer(11),
            'labelOptions' => $this->text(),
            'hint' => $this->text(),
            'items' => $this->text(),
            'value' => $this->text(),
            'widgetClass' => $this->string(255),
            'options' => $this->text(),
            'fieldConfig' => $this->text(),
            'columnOptions' => $this->text(),
            'columns' => $this->string(255),
            'order' => $this->integer(11),
            'status' => $this->integer(11),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            ], $tableOptions);
                
    }

    public function down()
    {
        $this->dropTable('core_forms');
    }
}
