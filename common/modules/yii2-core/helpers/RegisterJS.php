<?php 
namespace msoft\helpers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Component;

class RegisterJS extends Component{

    public function regis($assets,$view){
        $asset = [];
        if(!empty($assets) && is_array($assets)){
            foreach($assets as $val){
                $asset = ArrayHelper::merge($asset, [RegisterJS::getAssetClass($val,$view)]);
            }
            return $asset;
        }
    }

    public function getAssetClass($key,$view){
        $items = [
            'sweetalert' => eval("msoft\core\assets\sweetalert2\SweetAlertAsset::register(\$view);"),
            'bootbox' => eval("msoft\core\assets\bootbox\BootBoxAsset::register(\$view);"),
            'ajaxcrud' => eval("msoft\core\assets\ajaxcrud\CrudAsset::register(\$view);"),
            //'growl' => eval("msoft\core\assets\growl\GrowlAsset::register(\$view);"),
            'codemirror' => eval("msoft\core\assets\codemirror\CodeMirrorAsset::register(\$view);"),
        ];
        return ArrayHelper::getValue($items,$key,[]);
    }
}