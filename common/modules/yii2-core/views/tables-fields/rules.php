<?php 
use msoft\helpers\Html;
use msoft\widgets\TypeaheadBasic;
use msoft\helpers\RegisterJS;
use msoft\widgets\ActiveForm;

$this->title = 'Tables Rules';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sdbox-header">
    <h3><?= $this->title ?></h3>
</div>
<?php 
    $form = ActiveForm::begin([
        'id' => 'login-form-vertical', 
        'type' => ActiveForm::TYPE_VERTICAL
    ]); 
?>
    <p>
    <?php 
    echo TypeaheadBasic::widget([
        'name' => 'tables-name',
        'id' => 'input-tables',
        'data' =>  $tables,
        'dataset' => ['limit' => 20],
        'options' => ['placeholder' => 'Filter table name ...'],
        'pluginOptions' => ['highlight' => true, 'minLength' => 0],
    ]);
    ?>
    </p>
    <div class="form-group">
        <?= Html::a('Get Rule',false,['class' => 'btn btn-primary','onclick' => 'Rules.GetRules(this);']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-danger']) ?>
    </div>
<?php ActiveForm::end(); ?>
<p>
    <pre id="rules-code">
    </pre>
</p>
<?php
RegisterJS::regis(['sweetalert'],$this);

$this->registerJs(<<<JS
Rules = {
    GetRules: function(){
        var table  = $('input[id="input-tables"]').val();
        $.ajax({
				"type":"GET",
				"url":"/core/tables-fields/get-rules",
				"data":{table:table},
				"async": false,
				"dataType":"json",
				"success":function (result) {
					$('#rules-code').html(result);
				},
				"error":function (xhr, status, error) {
					swal(error, status, "error");
				}
		});
    }
};
JS
);?>