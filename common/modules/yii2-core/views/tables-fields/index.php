<?php

use yii\widgets\Pjax;
use yii\helpers\Url;
use msoft\widgets\GridView;
use msoft\core\widgets\ModalForm;
use msoft\helpers\Noty;
use yii\helpers\ArrayHelper;
use msoft\core\models\CoreFields;
use msoft\core\classes\CoreFunc;
use msoft\helpers\Html;
use msoft\core\assets\CoreAsset;
use msoft\helpers\RegisterJS;
use msoft\widgets\SwalAlert;
use msoft\core\models\TablesFields;

RegisterJS::regis(['codemirror'],$this);
CoreAsset::register($this);
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\core\models\TablesFieldsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('core', 'Tables Fields');
$this->params['breadcrumbs'][] = $this->title;
echo SwalAlert::widget();
?>
<style>
.modal-dialog {
    width: 80%;
}
.modal{
	z-index:1600;
}
</style>
<div class="tables-fields-index">

    <div class="sdbox-header">
		<h3>Tables Fields<?php //  Html::encode($this->title).' #'. CoreFunc::itemAlias('tables_fields', $table) ?></h3>
    </div>
    <?php /*
    <p style="padding-top: 10px;">
		<span class="label label-primary">Notice</span>
		<?= Yii::t('app', 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.') ?>
    </p>
	*/?>

    <?php  Pjax::begin(['id'=>'tables-fields-grid-pjax']);?>
    <?= GridView::widget([
		'id' => 'tables-fields-grid',
		'panel' => [
        	'heading'=> false,
			'before'=>	Html::a(Html::getBtnAdd(),['tables-fields/create'],['class' => 'btn btn-success btn-sm','data-pjax' => 0]).'  '.
						//Html::button(Html::getBtnAdd(), ['data-url'=>Url::to(['tables-fields/create'/*, 'table'=>$table*/]), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-tables-fields']). ' ' .
					  	Html::button(Html::getBtnDelete(), ['data-url'=>Url::to(['tables-fields/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-tables-fields', 'disabled'=>true]),
			'after'=>false,
		],
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'hover' => true,
		//'pjax' => true,
		'condensed' => true,
		'export' => [
			'fontAwesome' => true
		],
		'columns' => [
			[
				'class' => 'msoft\widgets\grid\CheckboxColumn',
				'checkboxOptions' => [
					'class' => 'selectionTablesFieldIds'
				],
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:40px;text-align: center;'],
			],
			[
				'class' => 'yii\grid\SerialColumn',
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:60px;text-align: center;'],
			],
			[
				'attribute'=>'action_id',
                'value'=>function ($model, $key, $index, $widget) { 
                    return $model['action_id'];
                },
                'filterType'=>GridView::FILTER_SELECT2,
                'filter'=>ArrayHelper::map(TablesFields::find()->orderBy('table_id')->asArray()->all(), 'action_id', 'action_id'), 
                'filterWidgetOptions'=>[
                    'pluginOptions'=>['allowClear'=>true],
                ],
				'filterInputOptions'=>['placeholder'=>'Action'],
				'group'=>true,  // enable grouping,
				'format' => 'raw'
			],
			[
				'attribute'=>'status',
				'class'=>'\msoft\widgets\ToggleColumn',
				'linkTemplateOn'=>'<a class="toggle-column btn btn-success btn-xs btn-block" data-pjax="0" href="{url}"><i  class="glyphicon glyphicon-ok"></i> {label}</a>',
				'linkTemplateOff'=>'<a class="toggle-column btn btn-danger btn-xs btn-block" data-pjax="0" href="{url}"><i  class="glyphicon glyphicon-remove"></i> {label}</a>'
			],
			[
				'attribute'=>'input_label',
				'contentOptions'=>['style'=>'width:150px;'],
			],
			[
				'attribute'=>'table_varname',
				'contentOptions'=>['style'=>'width:150px;'],
			],
			[
				'attribute'=>'table_field_type',
				'contentOptions'=>['style'=>'width:120px;'],
				'filter'=>Html::activeDropDownList($searchModel, 'table_field_type', CoreFunc ::itemAlias('field_type'), ['class'=>'form-control', 'prompt'=>'All']),
			],
			[
				'attribute'=>'table_length',
				'contentOptions'=>['style'=>'width:100px;'],
			],
			'table_default',
			[
				'attribute'=>'input_field',
				'contentOptions'=>['style'=>'width:160px;'],
				'filter'=>Html::activeDropDownList($searchModel, 'input_field', ArrayHelper::map(CoreFields::find()->all(), 'field_code', 'field_code'), ['class'=>'form-control', 'prompt'=>'All']),
			],
			[
				'attribute'=>'input_required',
				'value'=>function ($data){return ($data['input_required']===1)?'Yes':'No';},
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:60px;text-align: center;'],
				'filter'=>Html::activeDropDownList($searchModel, 'input_required', [1=>'Yes', 0=>'No'], ['class'=>'form-control', 'prompt'=>'All']),
			],
			[
				'class'=>'msoft\widgets\EditableColumn',
				'attribute'=>'input_order', 
				'editableOptions'=>[
					'header'=>'Oder', 
					'inputType'=>\msoft\widgets\Editable::INPUT_TEXT,
					'options'=>[
						'pluginOptions'=>[]
					],
					'asPopover' => false,
					'formOptions' => ['action' => ['/core/tables-fields/save-editable','attr' => 'input_order']],
				],
				'vAlign'=>'middle',
				'hAlign' => 'center'
			],
			/*
			[
				'attribute'=>'updated_at',
				'value'=>function ($data){return Yii::$app->formatter->asDate($data['updated_at'], 'd/M/Y');},
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:100px;text-align: center;'],
				'filter'=>'',
			],*/
			[
                'class' => 'msoft\widgets\ActionColumn',
                'template'=>'<div class="btn-group btn-group-sm" style="width:100px;"> {view} {update} {delete} </div>',
                'viewOptions' => [
					'class' => 'btn btn-sm btn-default',
					'data-action' => 'view'
                ],
                'updateOptions' => [
					'class' => 'btn btn-sm btn-default',
					'data-action' => 'update'
                ],
                'deleteOptions' => [
					'class' => 'btn btn-sm btn-danger',
					'data-action' => 'delete'
				],
			],
			
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-tables-fields',
    'size' => 'modal-lg',
	'options' => ['tabindex' => false,],
]);
?>

<?php  $this->registerJs("
$('#tables-fields-grid-pjax').on('click', '#modal-addbtn-tables-fields', function(){
    modalTablesField($(this).attr('data-url'));
});

$('#tables-fields-grid-pjax').on('click', '#modal-delbtn-tables-fields', function(){
    selectionTablesFieldGrid($(this).attr('data-url'));
});

$('#tables-fields-grid-pjax').on('click', '.select-on-check-all', function(){
    window.setTimeout(function() {
		var key = $('#tables-fields-grid').yiiGridView('getSelectedRows');
		disabledTablesFieldBtn(key.length);
    },100);
});

$('#tables-fields-grid-pjax').on('click', '.selectionTablesFieldIds', function(){
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledTablesFieldBtn(key.length);
});

$('#tables-fields-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalTablesField('".Url::to(['tables-fields/update', 'id'=>''])."'+id);
});
$('#tables-fields-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
	var action = $(this).attr('data-action');

    if(action === 'view'){
		modalTablesField(url);
	}else if(action === 'update'){ 
		window.location.href = url;
	}else if(action === 'delete') {
		yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function(){
			$.post(
				url
			).done(function(result){
				if(result.status == 'success'){
					". Noty::show('result.message', 'result.status') ."
					$.pjax.reload({container:'#tables-fields-grid-pjax'});
				} else {
					". Noty::show('result.message', 'result.status') ."
				}
			}).fail(function(){
				". Noty::show("'" . Html::getMsgError() . "Server Error'", '"error"') ."
				console.log('server error');
			});
		})
    }
    return false;
});
function disabledTablesFieldBtn(num){
    if(num>0){
		$('#modal-delbtn-tables-fields').attr('disabled', false);
    } else{
		$('#modal-delbtn-tables-fields').attr('disabled', true);
    }
}

function selectionTablesFieldGrid(url){
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function(){
		$.ajax({
			method: 'POST',
			url: url,
			data: $('.selectionTablesFieldIds:checked[name=\"selection[]\"]').serialize(),
			dataType: 'JSON',
			success: function(result, textStatus) {
				if(result.status == 'success') {
					". Noty::show('result.message', 'result.status') ."
					$.pjax.reload({container:'#tables-fields-grid-pjax'});
				} else {
					". Noty::show('result.message', 'result.status') ."
				}
			}
		})
	
    })
}

function modalTablesField(url) {
    $('#modal-tables-fields .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-tables-fields').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>