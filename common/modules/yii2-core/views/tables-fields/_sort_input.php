<?php
use msoft\widgets\SortableInput;
use msoft\widgets\ActiveForm;
use msoft\helpers\Html;
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]);
echo SortableInput::widget([
    'name'=> 'sort_list',
    'items' => $items,
    'hideInput' => true,
    'hideInput' => false,
    'options' => ['class'=>'form-control', 'readonly'=>true,'id' => 'sort']
]);
ActiveForm::end();
?>