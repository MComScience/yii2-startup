<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model msoft\core\models\CoreForms */

$this->title = 'Update Core Forms: ' . $model->form_id;
$this->params['breadcrumbs'][] = ['label' => 'Core Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->form_id, 'url' => ['view', 'id' => $model->form_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="core-forms-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
