<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model msoft\core\models\CoreForms */

$this->title = '# '.$model->attributes;
$this->params['breadcrumbs'][] = ['label' => 'Core Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-forms-view">
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'form_id',
            'row',
            'action_id',
            'attributes',
            'label',
            'type',
            'visible',
            'labelSpan',
            'labelOptions:ntext',
            'hint:ntext',
            'items:ntext',
            'widgetClass',
            'options:ntext',
            'columnOptions:ntext',
            'columns',
            'order',
        ],
    ]) ?>

    </div>
</div>
