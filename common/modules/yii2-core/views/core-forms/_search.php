<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model msoft\core\models\CoreFormsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="core-forms-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'form_id') ?>

    <?= $form->field($model, 'row') ?>

    <?= $form->field($model, 'action_id') ?>

    <?= $form->field($model, 'attributes') ?>

    <?= $form->field($model, 'label') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'visible') ?>

    <?php // echo $form->field($model, 'labelSpan') ?>

    <?php // echo $form->field($model, 'labelOptions') ?>

    <?php // echo $form->field($model, 'hint') ?>

    <?php // echo $form->field($model, 'items') ?>

    <?php // echo $form->field($model, 'widgetClass') ?>

    <?php // echo $form->field($model, 'options') ?>

    <?php // echo $form->field($model, 'columnOptions') ?>

    <?php // echo $form->field($model, 'columns') ?>

    <?php // echo $form->field($model, 'order') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
