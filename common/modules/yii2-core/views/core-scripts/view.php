<?php

use yii\helpers\Html;
use msoft\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model metronic\js\models\TbJsScripts */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tb Js Scripts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-js-scripts-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'url:url',
            'code:ntext',
            'created_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
