<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model metronic\js\models\TbJsScripts */

$this->title = 'Create Tb Js Scripts';
$this->params['breadcrumbs'][] = ['label' => 'Tb Js Scripts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-js-scripts-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
