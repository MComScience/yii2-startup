<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model metronic\core\models\CoreGenerate */

$this->title = 'Create Core Generate';
$this->params['breadcrumbs'][] = ['label' => 'Core Generates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-generate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
