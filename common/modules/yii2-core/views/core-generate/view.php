<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model metronic\core\models\CoreGenerate */

$this->title = $model->gen_id;
$this->params['breadcrumbs'][] = ['label' => 'Core Generates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-generate-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->gen_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->gen_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'gen_id',
            'gen_group',
            'gen_name',
            'gen_tag:ntext',
            'gen_link:ntext',
            'gen_process:ntext',
            'gen_ui:ntext',
            'template_php:ntext',
            'template_html:ntext',
            'template_js:ntext',
            'updated_at',
            'updated_by',
            'created_at',
            'created_by',
        ],
    ]) ?>

</div>
