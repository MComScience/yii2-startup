<?php

use yii\widgets\ActiveForm;
use msoft\helpers\Noty;
use msoft\helpers\Html;
/* @var $this yii\web\View */
/* @var $model backend\modules\core\models\CoreOptions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="core-options-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>
    <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="itemModalLabel">Core Options</h4>
	</div>
    <div class="modal-body">
    <?= $form->field($model, 'option_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'option_value')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'autoload')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'input_label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'input_hint')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'input_field')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'input_specific')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'input_data')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'input_required')->textInput() ?>

    <?= $form->field($model, 'input_validate')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'input_meta')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'input_order')->textInput() ?>
    </div>
    <div class="modal-footer">
	    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>
    <?php ActiveForm::end(); ?>

</div>
<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
		\$form.attr('action'), //serialize Yii2 form
		\$form.serialize()
    ).done(function(result){
		if(result.status == 'success'){
			". Noty::show('result.message', 'result.status') ."
			if(result.action == 'create'){
				$(\$form).trigger('reset');
				$.pjax.reload({container:'#core-fields-grid-pjax'});
			} else if(result.action == 'update'){
				$(document).find('#modal-core-fields').modal('hide');
				$.pjax.reload({container:'#core-fields-grid-pjax'});
			}
		} else{
			". Noty::show('result.message', 'result.status') ."
		} 
    }).fail(function(){
		". Noty::show("'" . Html::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
    });
    return false;
});

");?>