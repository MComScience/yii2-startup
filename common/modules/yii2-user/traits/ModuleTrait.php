<?php


namespace msoft\user\traits;

use msoft\user\Module;

/**
 * Trait ModuleTrait
 * @property-read Module $module
 * @package msoft\user\traits
 */
trait ModuleTrait
{
    /**
     * @return Module
     */
    public function getModule()
    {
        return \Yii::$app->getModule('user');
    }
}
