<?php

use yii\helpers\Html;
use msoft\widgets\ActiveForm;
use msoft\menu\models\Menu;
use msoft\menu\models\MenuCategory;
use msoft\widgets\Select2;
use msoft\widgets\SymbolPicker;
use msoft\menu\assets\AppAsset;
use msoft\widgets\Icon;
use yii\web\JsExpression;
use yii\web\View;

$asset = AppAsset::register($this);
$icons  = '';
$format = <<< SCRIPT
$.get('/dist/icons.yml', function(data){    
    var parsedYaml = jsyaml.load(data);
    $.each(parsedYaml.icons, function(index, icon){
        $('#menu-icon').append(
            '<option value="' + icon.id + '">'+ icon.id + '</option>'
        );
    });
});

function format(state) {
    if (!state.id) return state.text; // optgroup
    return '<i class="fa fa-'+state.text+'"></i> ' + state.text;
}
SCRIPT;
$escape = new JsExpression("function(m) { return m; }");
$this->registerJs($format, View::POS_HEAD);
?>
<?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

<div class="row">   
    <div class="col-sm-3">
        <?= $form->field($model, 'icon')->widget(Select2::classname(),[
            'options' => [
                'placeholder' => 'Select Icons ...',
            ],
            'pluginOptions' => [
                'templateResult' => new JsExpression('format'),
                'templateSelection' => new JsExpression('format'),
                'escapeMarkup' => $escape,
                'allowClear' => true
            ],
        ]) ?>
    </div>   
    <div class="col-sm-9">   

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="row">   
    <div class="col-sm-2">  
        <?= $form->field($model, 'target')->textInput(['maxlength' => true]) ?>
    </div>   
    <div class="col-sm-6">  
        <?= $form->field($model, 'router')->textInput(['maxlength' => true]) ?>
    </div>   
    <div class="col-sm-4">
        <?= $form->field($model, 'parameter')->textInput(['maxlength' => true]) ?>
    </div> 
</div>
<div class="row"> 
    <div class="col-sm-12">  
        <?= $form->field($model, 'route')->textArea(['rows' => 4]) ?>
    </div>   
</div>   

<div class="row"> 
    <div class="col-sm-12">  
        <?= $form->field($model, 'options')->textArea(['rows' => 4]) ?>
    </div>   
</div>   

<div class="row">   
    <div class="col-sm-6">
        <?= $form->field($model, 'menu_category_id')->dropDownList(MenuCategory::getList(), ['prompt' => Yii::t('app', 'เลือก')]) ?>
    </div>   
    <div class="col-sm-6">  
        <?= $form->field($model, 'parent_id')->dropDownList($model->getList(), ['prompt' => Yii::t('app', 'เลือก')]) ?>
    </div> 
</div> 


<div class="row">   
    <div class="col-sm-3">
        <?= $form->field($model, 'status')->dropDownList(Menu::getItemStatus(), ['prompt' => Yii::t('app', 'เลือก')]) ?>
    </div>   

    <div class="col-sm-3">  
        <?=
        $form->field($model, 'auth_items')->widget(Select2::ClassName(), [
            'data' => $model->getAuth(),
            'options' => [
                'placeholder' => 'Select a color ...',
                'multiple' => true
            ],
            'pluginOptions' => [
                //'allowClear' => true,
                'tags' => true,
                //'tokenSeparators' => [',', ' '],
                'maximumInputLength' => 10
            ],
        ])
        ?>
    </div>   
    <div class="col-sm-3">  
        <?= $form->field($model, 'protocol')->textInput(['maxlength' => true]) ?>
    </div>   
    <div class="col-sm-3">  
        <?= $form->field($model, 'home')->dropDownList([ 1 => '1', 0 => '0',], ['prompt' => '']) ?>
    </div>   
</div>   



<div class="row">   
    <div class="col-sm-3">

        <?php /* = $form->field($model, 'sort')->dropDownList(Menu::getSortBy($model->menu_category_id, $model->parent_id), ['prompt' => Yii::t('app', 'เลือก')]) */ ?>
        <?= $form->field($model, 'sort')->textInput() ?>
    </div>   
    <div class="col-sm-3">  

        <?php /* = $form->field($model, 'language')->textInput(['maxlength' => true]) ?>

          <?= $form->field($model, 'assoc')->textInput(['maxlength' => true]) */ ?>

        <?= $form->field($model, 'params')->textInput() ?> 

    </div> 
    <div class="col-sm-4">  
        <?= $form->field($model, 'module_id')->dropDownList($model->getModules(), ['prompt' => 'เลือก Module']) ?>
    </div>  
</div>  

<?php ActiveForm::end(); ?>



