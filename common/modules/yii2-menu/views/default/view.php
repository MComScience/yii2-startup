<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model msoft\menu\models\Menu */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('menu', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class='box box-info'>
    <div class='box-header'>
     <!-- <h3 class='box-title'><?= Html::encode($this->title) ?></h3>-->
    </div><!--box-header -->
    
    <div class='box-body pad'>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'menu_category_id',
            'parent_id',
            'title',
            'router',
            'parameter',
            'icon',
            'status',
            'item_name',
            'target',
            'protocol',
            'home',
            'sort',
            'language',
            'assoc',
            'created_at',
            'created_by',
        ],
    ]) ?>


    </div><!--box-body pad-->
 </div><!--box box-info-->
