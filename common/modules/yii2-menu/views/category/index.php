<?php

use yii\helpers\Html;
use msoft\widgets\Datatables;
use yii\widgets\Pjax;
use msoft\widgets\Icon;
use msoft\helpers\RegisterJS;
use msoft\widgets\swalalert\SwalAlert;
use msoft\widgets\Panel;
RegisterJS::regis(['sweetalert'],$this);

$this->title = Yii::t('menu', 'จัดการประเภทเมนู');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= SwalAlert::widget(); ?>

<div class="row">
    <div class="col-sm-2">
        <?php echo $this->render('../layouts/tabs-menu.php',['active' => 'จัดการประเภทเมนู']); ?>
    </div>
    <div class="col-sm-10">
        <div class="tab-content printable">
            <div id="tabs1" class="tab-pane fade active in">
                <?php echo Panel::begin([
                    'title'=> $this->title,
                    'type' => 'default',
                    'widget'=>false
                ]); ?>
                    <?php Pjax::begin(["id" => "crud-datatable-pjax"]); ?> 
                    <?= Datatables::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'hover' => true,
                        'bordered' => false,
                        'condensed' => true,
                        'striped' => true,
                        'export' => false,
                        'clientOptions' =>[
                            'language' => [
                                "search" => "ค้นหา: _INPUT_". ' '. Html::a(Icon::show('plus').' Add', ['create'], ['class' => 'btn btn-primary','role'=>'modal-remote']),
                            ]
                        ],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',
                            'title',
                            'discription',
                            'status',
                            [
                                'class' => 'msoft\menu\widgets\ActionColumn',
                                'contentOptions' => ['style' => 'text-align:center;'],
                                'viewOptions' => [
                                    'role' => 'modal-remote'
                                ],
                                'updateOptions' => [
                                    'role' => 'modal-remote'
                                ]
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                <?php Panel::end(); ?>
            </div>
        </div>
    </div>
</div>

<?php echo $this->render('../default/modal.php'); ?>

