<?php

use msoft\helpers\Html;
use msoft\widgets\Panel;
use msoft\widgets\Icon;
use msoft\helpers\RegisterJS;
use msoft\widgets\SwalAlert;
use msoft\widgets\GridView;
use msoft\widgets\Datatables;

/* @var $this yii\web\View */
/* @var $searchModel msoft\core\models\TablesFieldsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
RegisterJS::regis(['sweetalert'],$this);

$this->title = Yii::t('app', 'Tables Fields');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('
    .btn-default {
        background : #fff !important;
    }
');
?>
<?= SwalAlert::widget(); ?>
<?= Panel::begin([
    'title'=> 'Demo Crud Template (from CRUD Generator)',
    'type' => Panel::TYPE_PRIMARY,
    'footer' => 'Footer'
]);
?>
<div class="tables-fields-index">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= Datatables::widget([
        'dataProvider' => $dataProvider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'clientOptions' => [
            'language' => [
                "search" => "ค้นหา: _INPUT_" .'&nbsp;'. Html::a(Icon::show('plus'),['create'],['class' => 'btn btn-default','role' => 'modal-remote']),
            ],
            /*"dom"=> "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            'buttons' => [
                ['extend' => 'print','className' => 'btn dark btn-outline'],
                ['extend' => 'copy','className' => 'btn red btn-outline'],
                ['extend' => 'pdf','className' => 'btn green btn-outline'],
                ['extend' => 'excel','className' => 'btn yellow btn-outline'],
                ['extend' => 'csv','className' => 'btn purple btn-outline'],
                ['extend' => 'colvis','className' => 'btn dark btn-outline'],
            ],*/
        ],
        'hover' => true,
        'datatables' => true,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'msoft\widgets\grid\SerialColumn'],
            [
                'class'=>'msoft\widgets\grid\ExpandRowColumn',
                'width'=>'50px',
                'value'=>function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'headerOptions'=>['class'=>'kartik-sheet-style'] ,
                'expandOneOnly'=>true,
                'detailUrl' => yii\helpers\Url::to(['/data-expand'])
            ],
            'table_id',
            'table_name',
            'table_varname',
            'table_field_type',
            'table_length',
            // 'table_default:ntext',
            // 'table_index',
            // 'input_field',
            // 'input_label',
            // 'input_hint:ntext',
            // 'input_specific:ntext',
            // 'input_data:ntext',
            // 'input_required',
            // 'input_validate:ntext',
            // 'input_meta:ntext',
            // 'input_order',
            // 'action_id',
            // 'begin_html:ntext',
            // 'end_html:ntext',
            // 'updated_at',
            // 'updated_by',
            // 'created_at',
            // 'created_by',
            // 'status',

            [
                'class' => 'msoft\widgets\ActionColumn',
                'noWrap' => TRUE,
                'viewOptions' => [
                    'class' => 'btn btn-xs btn-success tooltips',
                    'role' => 'modal-remote'
                ],
                'updateOptions' => [
                    'class' => 'btn btn-xs btn-primary tooltips',
                    'role' => 'modal-remote'
                ],
                'deleteOptions' => [
                    'class' => 'btn btn-xs btn-danger tooltips',
                ],
            ],
        ],
    ]); ?>
</div>
<?= Panel::end(); ?>

<?php echo $this->render('modal'); ?>