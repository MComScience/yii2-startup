<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log','msoft\user\Bootstrap'],
    'homeUrl' => '/app-backend',
    'modules' => [
        'user' => [
            'class' => 'msoft\user\Module',
            'enableConfirmation' => false,
            'enableFlashMessages' => false,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['admin'],
        ],
    ],
    'components' => [
        'fileStorage'=>[
            'class' => 'trntv\filekit\Storage',
            'baseUrl' => '@web/uploads',   
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
            'dateFormat' => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'timeFormat' => 'php:H:i:s',
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/app-backend',
        ],
        'user' => [
            'identityClass' => 'msoft\user\models\User',
            'identityCookie' => [
                'name'     => '_backendIdentity',
                'httpOnly' => true,
            ],
            'enableAutoLogin' => true,
        ],
        'session' => [
            'name' => 'BACKENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
            ],
        ], 
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                   '@app/views' => '@app/themes/adminlte'
                ],
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-blue',
                    /*
                    "skin-blue",
                    "skin-black",
                    "skin-red",
                    "skin-yellow",
                    "skin-purple",
                    "skin-green",
                    "skin-blue-light",
                    "skin-black-light",
                    "skin-red-light",
                    "skin-yellow-light",
                    "skin-purple-light",
                    "skin-green-light"
                    */
                ],
            ],
        ],
    ],
    'as access' => [
        'class' => 'msoft\admin\components\AccessControl',
        'allowActions' => [
            'site/*',
            'admin/*',
            'user/registration/*',
            'user/security/*',
            'user/recovery/*'
        ]
    ],
    'params' => $params,
];
